<?php

namespace App\Http\Livewire;

use App\Contact;
use Livewire\Component;

class ContactUpdate extends Component
{
    public $name;
    public $phone;
    public $email;
    public $contactId;

    protected $listeners = [
        'dataEdit' => 'showUpdateForm'
    ];

    public function render()
    {
        return view('livewire.contact-update');
    }

    public function showUpdateForm($users) {
        $this->name = $users['name'];
        $this->phone = $users['phone'];
        $this->email = $users['email'];
        $this->contactId = $users['id'];
    }

    public function update() {
        $this->validate([
            'name' => 'required|min:3',
            'phone' => 'required|max:14',
            'email' => 'required'
        ]);

        if ($this->contactId) {
            $update = Contact::find($this->contactId);

            $update->update([
                'name' => $this->name,
                'phone' => $this->phone,
                'email' => $this->email,
            ]);
    
            $this->resetUpdate();
    
            $this->emit('updated', $update);
        }
    }

    private function resetUpdate() {
        $this->name = null;
        $this->phone = null;
        $this->email = null;
    }
}
