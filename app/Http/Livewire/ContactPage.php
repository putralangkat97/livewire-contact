<?php

namespace App\Http\Livewire;

use App\Contact;
use Livewire\Component;

class ContactPage extends Component
{
    public $statusUpdate = false;

    protected $listeners = [
        'stored' => 'handleStore',
        'updated' => 'handleUpdate'
    ];

    public function render()
    {
        $data = Contact::latest()->get();
        return view('livewire.contact-page')->with('data', $data);
    }

    public function dataEdit($id) {
        $this->statusUpdate = true;
        $users = Contact::findOrFail($id);

        $this->emit('dataEdit', $users);
    }

    public function handleStore($create) {
        // dd($create);
        session()->flash('message', $create['name'].' was created successfully!');
    }

    public function handleUpdate($update) {
        // dd($create);
        session()->flash('message', $update['name'].' info was updated successfully!');
    }
}
