<?php

namespace App\Http\Livewire;

use App\Contact;
use Livewire\Component;

class ContactCreate extends Component
{
    public $name;
    public $phone;
    public $email;

    public function render()
    {
        return view('livewire.contact-create');
    }

    public function add() {
        $this->validate([
            'name' => 'required|min:3',
            'phone' => 'required|max:14',
            'email' => 'required'
        ]);

        $create = Contact::create([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email
        ]);

        $this->resetInput();

        $this->emit('stored', $create);
    }

    private function resetInput() {
        $this->name = null;
        $this->phone = null;
        $this->email = null;
    }
}
