<div class="row mt-3">
    <div class="container">
        <div class="col-lg-10 offset-lg-1 col-md-12">
        @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('message') }}</strong>
            </div>
        @else
            
        @endif
        </div>
        @if ($statusUpdate)
            <livewire:contact-update></livewire:contact-update>
        @else
            <livewire:contact-create></livewire:contact-create>
        @endif
        <div class="col-lg-10 offset-lg-1 col-md-12 col-12">
            <table class="table table-bordered table-hovered table-striped">
                <thead class="text-center bg-warning">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone Numbers</th>
                        <th>E-mail</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @php
                        $no = 0;
                    @endphp
                    @foreach ($data as $d)
                    @php
                        $no++;
                    @endphp
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->phone }}</td>
                        <td>{{ $d->email }}</td>
                        <td>
                            <button wire:click="dataEdit({{ $d->id }})" class="btn btn-rounded btn-info"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="white" width="18px" height="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/></svg></button>
                            &nbsp;
                            <button class="btn btn-rounded btn-danger"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z"/></svg></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>