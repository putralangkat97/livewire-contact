<div class="col-lg-10 offset-lg-1 col-md-12">
    <form wire:submit.prevent="add">
        <div class="row">
            <div class="col">
                <h4>
                    <span>Create</span>
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input wire:model="name" type="text" name="" id="" placeholder="Name .." class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <input wire:model="phone" type="text" name="" id="" placeholder="Phone Numbers .." class="form-control @error('phone') is-invalid @enderror">
                    @error('phone')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input wire:model="email" type="email" name="" id="" placeholder="E-mail .." class="form-control @error('email') is-invalid @enderror">
                    @error('email')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success float-right">Create</button>
        <br>
        <br>
    </form>
</div>
